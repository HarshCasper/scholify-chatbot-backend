# Scholify-Chatbot-Backend

This Repository consists of the Backend of the Chatbot that has been developed using LSTM Neural Networks.

Download the Model.h5 File (The Brain of the Backend) from [here](https://drive.google.com/file/d/1ZxYsovcwdlIFL_DN0J4M-wTo3yzvpTPP/view?usp=sharing)